#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>


void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}


std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  glGenBuffers(model.buffers.size(), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); i++) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(),
        model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return bufferObjects;
}

bool getPosAndTextCoords(const tinygltf::Model &model, const tinygltf::Primitive primitive, std::vector<glm::vec3> &positions, std::vector<glm::vec2> &textCoords){
  
  //Get Positions
  const auto iterator = primitive.attributes.find("POSITION");
  if (iterator != end(primitive.attributes)) {
    const auto accessorIdx = (*iterator).second;
    const auto &accessor = model.accessors[accessorIdx]; // get the correct tinygltf::Accessor from model.accessors
    const auto &bufferView = model.bufferViews[accessor.bufferView]; // get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
    const auto bufferIdx = bufferView.buffer;// get the index of the buffer used by the bufferView (you need to use it)
    const auto buffer = model.buffers[bufferIdx];// get the correct buffer object from the buffer index
    const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;// Compute the total byte offset using the accessor and the buffer view
    const auto byteStride = bufferView.byteStride ? bufferView.byteStride : 3 * sizeof(float);
    const auto nbPos = accessor.count;

    //Get Texture Coordinates
    const auto iterator2 = primitive.attributes.find("TEXCOORD_0");
    if (iterator2 != end(primitive.attributes)) { 
      const auto textAccessorIdx = (*iterator2).second;
      const auto &textAccessor = model.accessors[textAccessorIdx]; 
      const auto &textBufferView = model.bufferViews[textAccessor.bufferView]; 
      const auto textBufferIdx = textBufferView.buffer;
      const auto textBuffer = model.buffers[textBufferIdx];
      const auto textByteOffset = textAccessor.byteOffset + textBufferView.byteOffset;
      const auto textByteStride = textBufferView.byteStride ? textBufferView.byteStride : 2 * sizeof(float);

      //Check primitive.indices (Base on code from computeSceneBounds in gltf.cpp)
      if(primitive.indices >= 0){
          const auto &indexAccessor = model.accessors[primitive.indices];
          const auto &indexBufferView = model.bufferViews[indexAccessor.bufferView];
          const auto indexByteOffset = indexAccessor.byteOffset + indexBufferView.byteOffset;
          const auto &indexBuffer = model.buffers[indexBufferView.buffer];
          auto indexByteStride = indexBufferView.byteStride;

        switch (indexAccessor.componentType) {
          default:
            std::cerr
              << "Primitive index accessor with bad componentType "
              << indexAccessor.componentType << ", skipping it."
              << std::endl;
              break;
          case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
            indexByteStride = indexByteStride ? indexByteStride : sizeof(uint8_t); break;
          case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
            indexByteStride = indexByteStride ? indexByteStride : sizeof(uint16_t); break;
          case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
            indexByteStride = indexByteStride ? indexByteStride : sizeof(uint32_t); break;
        }

        for (size_t i = 0; i < indexAccessor.count; ++i) {
          uint32_t index = 0;
          switch (indexAccessor.componentType) {
          case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
            index = *((const uint8_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
            break;
          case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
            index = *((const uint16_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
            break;
          case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
            index = *((const uint32_t *)&indexBuffer.data[indexByteOffset + indexByteStride * i]);
            break;
          }
          //Feed Positions
          const auto &pos = *((const glm::vec3 *)&buffer.data[byteOffset + byteStride * index]);
          positions.push_back(pos);

          //Feed TextCoords
          const auto &textCoord = *((const glm::vec2 *)&textBuffer.data[textByteOffset + textByteStride * index]);
          textCoords.push_back(textCoord);
        }
      }
      else{
        for(int i = 0; i < nbPos; i++){
          //Feed Positions
          const auto &pos = *((const glm::vec3 *)&buffer.data[byteOffset + byteStride * i]);
          positions.push_back(pos);
          //Feed TextCoords
          const auto &textCoord = *((const glm::vec2 *)&textBuffer.data[textByteOffset + textByteStride * i]);
          textCoords.push_back(textCoord);
        }  
      }
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }
  return true;
}

void ViewerApplication::createTangents(const tinygltf::Model &model, const tinygltf::Primitive primitive, int vertexAttribIdx){
      
  std::vector<glm::vec3> positions;
  std::vector<glm::vec2> textCoords;
	std::vector<glm::vec3> tangents;

  getPosAndTextCoords(model, primitive, positions, textCoords);

  //Calculate Tangents
  for (int i = 0; i < positions.size(); i += 3){
				glm::vec3 v1 = positions[i];
				glm::vec3 v2 = positions[i + 1];
				glm::vec3 v3 = positions[i + 2];

				glm::vec2 uv1 = textCoords[i];
				glm::vec2 uv2 = textCoords[i + 1];
				glm::vec2 uv3 = textCoords[i + 2];

				glm::vec3 edge1 = v2 - v1;
				glm::vec3 edge2 = v3 - v1;
				glm::vec2 deltaUV1 = uv2 - uv1;
				glm::vec2 deltaUV2 = uv3 - uv1;  

				float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
				float x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
				float y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
				float z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

				tangents.push_back(glm::vec3(x, y, z));
	}

  //Bind to Vao
  GLuint buffer;
	glGenBuffers(1, &buffer);
  glEnableVertexAttribArray(vertexAttribIdx);
  glBindBuffer(GL_ARRAY_BUFFER, buffer);
  glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), tangents.data(), GL_STATIC_DRAW);
  glVertexAttribPointer(vertexAttribIdx, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (const GLvoid *) 0);
  
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange){
  int VERTEX_ATTRIB_POSITION_IDX = 0;
  int VERTEX_ATTRIB_NORMAL_IDX = 1;
  int VERTEX_ATTRIB_TEXCOORD0_IDX = 2;
  int VERTEX_ATTRIB_TANGENT_IDX = 3;

  meshIndexToVaoRange.resize(model.meshes.size());
  
  std::vector<GLuint> vertexArrayObjects;
  for(size_t i = 0; i < model.meshes.size(); i++){
    const auto &mesh = model.meshes[i];
    int size = mesh.primitives.size();
    int offSet = vertexArrayObjects.size();
    VaoRange range = meshIndexToVaoRange[i];
    range.begin = offSet;
    range.count = size;
    vertexArrayObjects.resize(size + offSet);
    glGenVertexArrays(size, &vertexArrayObjects[offSet]);
    for(int id = 0; id < mesh.primitives.size(); id++){
      const auto& primitive = mesh.primitives[id];
      GLuint vao = vertexArrayObjects[range.begin + id];
      glBindVertexArray(vao);
      {
        const auto iterator = primitive.attributes.find("POSITION");
        if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; // get the correct tinygltf::Accessor from model.accessors
          const auto &bufferView = model.bufferViews[accessor.bufferView]; // get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
          const auto bufferIdx = bufferView.buffer;// get the index of the buffer used by the bufferView (you need to use it)

          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);// Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);// Bind the buffer object to GL_ARRAY_BUFFER

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;// Compute the total byte offset using the accessor and the buffer view
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, 
          accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), 
          (const GLvoid *) (byteOffset));// Call glVertexAttribPointer with the correct arguments.
          // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
          // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
        }
      }

      {
        const auto iterator = primitive.attributes.find("NORMAL");
        if (iterator != end(primitive.attributes)) { 
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView]; 
          const auto bufferIdx = bufferView.buffer;

          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, 
          accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), 
          (const GLvoid *) (byteOffset));
        }
      }

      {
        const auto iterator = primitive.attributes.find("TEXCOORD_0");
        if (iterator != end(primitive.attributes)) { 
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView]; 
          const auto bufferIdx = bufferView.buffer;

          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, 
          accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), 
          (const GLvoid *) (byteOffset));
        }
      }

      {
        const auto iterator = primitive.attributes.find("TANGENT");
        if (iterator != end(primitive.attributes)) { 
          const auto accessorIdx = (*iterator).second;
          const auto &accessor = model.accessors[accessorIdx]; 
          const auto &bufferView = model.bufferViews[accessor.bufferView]; 
          const auto bufferIdx = bufferView.buffer;


          glEnableVertexAttribArray(VERTEX_ATTRIB_TANGENT_IDX);
          glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

          const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
          glVertexAttribPointer(VERTEX_ATTRIB_TANGENT_IDX, accessor.type, 
          accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride), 
          (const GLvoid *) (byteOffset));
        }
        else{
          createTangents(model, primitive, VERTEX_ATTRIB_TANGENT_IDX);
        }


      }

      if(primitive.indices >= 0){
        const auto &accessor = model.accessors[primitive.indices];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;
        const auto bufferObject = bufferObjects[bufferIdx];

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObject);

      }
    }
  }
  glBindVertexArray(0);

  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const{

  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);

  // Generate the texture object:
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  
  for(size_t i = 0; i < model.textures.size(); i++){

    const auto &texture = model.textures[i]; // get i-th texture
    assert(texture.source >= 0); // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image
    const auto &sampler =
            texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);

    // fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
            GL_RGBA, image.pixel_type, image.image.data());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
      sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
      sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
      sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
      sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
      sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
        glGenerateMipmap(GL_TEXTURE_2D);
    }
  }      
  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;

}


int ViewerApplication::run()
{

  tinygltf::Model model;
  // Loading the glTF file
  loadGltfFile(model);

  // Load shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});


  // ====================== Uniform Locations =================================================

  // Matrix Location

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  //Light Location
  
  const auto lightDirectionLocation = glGetUniformLocation(glslProgram.glId(),"uLightDirection");
  const auto lightIntensityLocation = glGetUniformLocation(glslProgram.glId(),"uLightIntensity");

  //Texture Locations
  const auto baseColorTextureLocation = glGetUniformLocation(glslProgram.glId(),"uBaseColorTexture");
  const auto metallicRoughnessTextureLocation = glGetUniformLocation(glslProgram.glId(),"uMetallicRoughnessTexture");
  const auto emissiveTextureLocation = glGetUniformLocation(glslProgram.glId(),"uEmissiveTexture");
  const auto occlusionTextureLocation = glGetUniformLocation(glslProgram.glId(),"uOcclusionTexture");
  const auto normalMapTextureLocation = glGetUniformLocation(glslProgram.glId(),"uNormalMapTexture");
  
  //Base Color Factor Location
  const auto baseColorFactorLocation = glGetUniformLocation(glslProgram.glId(),"uBaseColorFactor");

  //Metallic and Roughness Factor Locations
  const auto metallicFactorLocation = glGetUniformLocation(glslProgram.glId(),"uMetallicFactor");
  const auto roughnessFactorLocation = glGetUniformLocation(glslProgram.glId(),"uRoughnessFactor");

  //Emissive Factor Location
  const auto emissiveFactorLocation = glGetUniformLocation(glslProgram.glId(),"uEmissiveFactor");

  //Occlusion Strength Location
  const auto occlusionStrengthLocation = glGetUniformLocation(glslProgram.glId(),"uOcclusionStrength");

  //Enable Occlusion Location 
  const auto enableOcclusionLocation = glGetUniformLocation(glslProgram.glId(),"uEnableOcclusion");

  //Normal Scale Location
  const auto normalScaleLocation = glGetUniformLocation(glslProgram.glId(),"uNormalScale");

  //Enable Normal Mapping Location
  const auto enableNormalMappingLocation = glGetUniformLocation(glslProgram.glId(),"uEnableNormalMapping");

  // ====================== End of Uniform Locations ============================================
  
  auto lightDirection = glm::vec3(1.,1.,1.);
  auto lightIntensity = glm::vec3(3.,3.,3.);
  bool enableOcclusion = true;
  bool enableNormalMapping = false;

  int cameraValue = 1;
  auto color = glm::vec3( 1., 1., 1.);
  float intensity = 8.f;
  bool cameraLight = false;
  lightIntensity = color * intensity;
  auto tmpDirection = lightDirection;
  float theta = 1.f, phi = 2.f, x = 1, y = 1, z = 1;

  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);

  const glm::vec3 center = 0.5f * (bboxMax + bboxMin);
  const glm::vec3 diagonal = bboxMax - bboxMin;
  const auto up = glm::vec3(0,1,0);
  const auto eye = diagonal.z > 0 ? center + diagonal : center + 2.f * glm::cross(diagonal, up);

  // Build projection matrix
  auto maxDistance = glm::length(diagonal); // TODO use scene bounds instead to compute this
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // Implement a new CameraController model and use it instead.

  std::unique_ptr<CameraController> cameraController = 
    std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
  
  if (m_hasUserCamera) {
    cameraController -> setCamera(m_userCamera);
  } else {
    cameraController-> setCamera(
        Camera{eye, center, up});
  }

  // ==================== Textures ================================================== 

  //  Creation of Texture Objects
  const auto textureObjects = createTextureObjects(model);

  // Create of white Texture
  GLuint whiteTexture = 0;

  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[] = {1, 1, 1,1};

  // Fill it
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  // ==================== End of Textures ================================================== 


  // Creation of Buffer Objects
  auto bufferObjects = createBufferObjects(model);

  // Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  auto vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  //Lambda function to bind material (Texture)
  const auto bindMaterial = [&](const auto materialIndex) {
    auto textureObject = whiteTexture;

    if (materialIndex >= 0){
      const auto &material = model.materials[materialIndex];  
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      //Base Color Factor
      if(baseColorFactorLocation >= 0){
        glUniform4f(baseColorFactorLocation,
          (float)pbrMetallicRoughness.baseColorFactor[0],
          (float)pbrMetallicRoughness.baseColorFactor[1],
          (float)pbrMetallicRoughness.baseColorFactor[2],
          (float)pbrMetallicRoughness.baseColorFactor[3]);
      }

      //Metallic Factor
      if(metallicFactorLocation >= 0){
        glUniform1f(metallicFactorLocation, (float)pbrMetallicRoughness.metallicFactor);
      }

      //Roughness Factor
      if(roughnessFactorLocation >= 0){
        glUniform1f(roughnessFactorLocation, (float)pbrMetallicRoughness.roughnessFactor);
      }

      //Emissive Factor
      if(emissiveFactorLocation >= 0){
        glUniform3f(emissiveFactorLocation, 
          (float)material.emissiveFactor[0],
          (float)material.emissiveFactor[1],
          (float)material.emissiveFactor[2]);
      }

      //Occlusion Strength
      if(occlusionStrengthLocation >= 0){
        glUniform1f(occlusionStrengthLocation, (float)material.occlusionTexture.strength);
      }

      //Normal Scale
      if(normalScaleLocation >= 0){
        glUniform1f(normalScaleLocation, (float)material.normalTexture.scale);
      }

      //Base Color Texture
      if(baseColorTextureLocation >= 0){
        if(pbrMetallicRoughness.baseColorTexture.index >= 0){
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
          if(texture.source >= 0){
            textureObject = textureObjects[texture.source];
          }
        }
        // Bind textureObject to target GL_TEXTURE_2D of texture unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        // By setting the uniform to 0, we tell OpenGL the texture is bound on tex unit 0:
        glUniform1i(baseColorTextureLocation, 0);
      }
      
      //Metallic Texture
      if(metallicRoughnessTextureLocation >= 0){
        textureObject = 0u;
        if(pbrMetallicRoughness.metallicRoughnessTexture.index >= 0){
          const auto &texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
          if(texture.source >= 0){
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(metallicRoughnessTextureLocation, 1);
      }

      //Emissive Texture
      if(emissiveTextureLocation >= 0){
        textureObject = 0u;
        if(material.emissiveTexture.index >= 0){
          const auto &texture = model.textures[material.emissiveTexture.index];
          if(texture.source >= 0){
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(emissiveTextureLocation, 2);
      }

      //Occlusion Texture
      if(occlusionTextureLocation >= 0){
        textureObject = whiteTexture;
        if(material.occlusionTexture.index >= 0){
          const auto &texture = model.textures[material.occlusionTexture.index];
          if(texture.source >= 0){
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(occlusionTextureLocation, 3);
      }

      //Normal Map Texture
      if(normalMapTextureLocation >= 0){
        textureObject = whiteTexture;
        if(material.normalTexture.index >= 0){
          const auto &texture = model.textures[material.normalTexture.index];
          if(texture.source >= 0){
            textureObject = textureObjects[texture.source];
          }
        }
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(occlusionTextureLocation, 4);
      }
    }

    else{
      //Base Color Factor
      if(baseColorFactorLocation >= 0){
        glUniform4f(baseColorFactorLocation, 1.0f, 1.0f, 1.0f, 1.0f);
      }

      //Metallic Factor
      if(metallicFactorLocation >= 0){
        glUniform1f(metallicFactorLocation, 1.0f);
      }

      //Roughness Factor
      if(roughnessFactorLocation >= 0){
        glUniform1f(roughnessFactorLocation, 1.0f);
      }

      //Emissive Factor
      if(emissiveFactorLocation >= 0){
        glUniform3f(emissiveFactorLocation, 0.f, 0.f, 0.0f);
      }

      //Occlusion Strength
      if(occlusionStrengthLocation >= 0){
        glUniform1f(occlusionStrengthLocation, 0.0f);
      }

      //Normal Scale
      if(normalScaleLocation >= 0){
        glUniform1f(normalScaleLocation, 1.0f);
      }

      //Base Color Texture
      if(baseColorTextureLocation >= 0){
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(baseColorTextureLocation, 0);
      }

      //Metallic Texture
      if(metallicRoughnessTextureLocation >= 0){
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(metallicRoughnessTextureLocation, 1);
      }

      //Emissive Texture
      if(emissiveTextureLocation >= 0){
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(emissiveTextureLocation, 2);
      }

      //Occlusion Texture
      if(occlusionTextureLocation >= 0){
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(occlusionTextureLocation, 3);
      }

      //Normal Map Texture
      if(normalMapTextureLocation >= 0){
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(normalMapTextureLocation, 4);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    const auto viewMatrix = camera.getViewMatrix();

    if(lightIntensityLocation >= 0){
      glUniform3f(lightIntensityLocation, lightIntensity.x, lightIntensity.y, lightIntensity.z);
    }

    if(lightDirectionLocation >= 0){
      if (cameraLight) {
        glUniform3f(lightDirectionLocation, 0, 0, 1);
      } 
      else {
        const auto normalizeDirection = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
        glUniform3f(lightDirectionLocation, normalizeDirection.x, normalizeDirection.y, normalizeDirection.z);
      }
    }

    //Enable Occlusion
    if(enableOcclusionLocation >= 0){
      glUniform1i(enableOcclusionLocation, enableOcclusion);
    }

    //Enable Normal Mapping
    if(enableNormalMappingLocation >= 0){
      glUniform1i(enableNormalMappingLocation, enableNormalMapping);
    }


    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const auto node = model.nodes[nodeIdx];
          const glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
          if(node.mesh >= 0){
            const auto modelViewMatrix = viewMatrix * modelMatrix;
            const auto modelViewProjMatrix = projMatrix * modelViewMatrix;
            const auto normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewProjMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

            const auto &mesh = model.meshes[node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[node.mesh];
            for(size_t i = 0 ; i < mesh.primitives.size(); i++){
              const auto &primitive = mesh.primitives[i];
              const auto &vertexArrayObject = vertexArrayObjects[vaoRange.begin + i];
              //Bind Material
              bindMaterial(primitive.material);
              //Bind VAO
              glBindVertexArray(vertexArrayObject);
              if(primitive.indices >= 0){
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const GLvoid *) byteOffset );
              }
              else{
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }

          for(const auto id: node.children){
            drawNode(id, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // Draw all nodes
      for(const auto nodeIdx : model.scenes[model.defaultScene].nodes){
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };

  if(!m_OutputPath.empty()){
    const GLsizei nbComponents = 3;
    const auto width = m_nWindowWidth;
    const auto height = m_nWindowHeight;
    std::vector<unsigned char> pixels(height * width * nbComponents);
    renderToImage(width, height, nbComponents, pixels.data(), [&]() {
      const auto camera = cameraController -> getCamera();
      drawScene(cameraController -> getCamera());
    });
    flipImageYAxis(width, height, nbComponents, pixels.data());

    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), width, height, nbComponents, pixels.data(), 0);
    return 0;
  }


  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController -> getCamera();
    drawScene(camera);

    // ======================================== GUI CODE ===========================================
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        bool switchCamera = ImGui::RadioButton("First person", &cameraValue, 0) || ImGui::RadioButton("Trackball", &cameraValue, 1);
        if(switchCamera){
          const auto cam = cameraController -> getCamera();
          if(cameraValue == 0){
            cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          else{
            cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController -> setCamera(cam);
        }

        if(ImGui::ColorEdit3("Color", (float *) &color) || ImGui::SliderFloat("Intensity", &intensity, 0., 15.)){
          lightIntensity =  color * intensity;
        }

        ImGui::Checkbox("Enable Occlusion", &enableOcclusion);

        ImGui::Checkbox("Enable Normal Mapping", &enableNormalMapping);

        ImGui::Checkbox("Light From Camera", &cameraLight);
        


        if(!cameraLight){
          x = sin(theta) * cos(phi);
          y = cos(theta);
          z = sin(theta) * sin(phi);
          
          if(ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)){
            bool thetaChange = ImGui::SliderFloat("Theta", &theta, 0, glm::pi<float>()) ;
            bool phiChange =ImGui::SliderFloat("phi", &phi, 0, 2 * (glm::pi<float>()));
            if(thetaChange || phiChange){
              x = sin(theta) * cos(phi);
              y = cos(theta);
              z = sin(theta) * sin(phi);
            }
          }
          
          lightDirection = glm::vec3(x, y, z);
        }
        
        
      }
      ImGui::End();
    }

    // ======================================== END OF GUI CODE ===========================================

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController -> update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
