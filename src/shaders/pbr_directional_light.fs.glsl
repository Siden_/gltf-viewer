#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

uniform sampler2D uBaseColorTexture;
uniform sampler2D uMetallicRoughnessTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uOcclusionTexture;

uniform vec4 uBaseColorFactor;
uniform float uMetallicFactor;
uniform float uRougnessFactor;
uniform vec3 uEmissiveFactor;
uniform float uOcclusionStrength;

uniform int uEnableOcclusion;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main()
{
  vec3 N = normalize(vViewSpaceNormal);
  vec3 L = uLightDirection;
  vec3 V = normalize(-vViewSpacePosition);
  vec3 H = normalize(L + V);

  vec4 baseColorFromTexture = SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
  vec4 baseColor = uBaseColorFactor * baseColorFromTexture;

  vec4 metallicRoughnessFromTexture =
    texture(uMetallicRoughnessTexture, vTexCoords);

  vec3 metallic = vec3(uMetallicFactor * metallicRoughnessFromTexture.b);
  float roughness = uRougnessFactor * metallicRoughnessFromTexture.g;

  vec3 dielectric = vec3(0.04);
  vec3 black = vec3(0.);

  vec3 c_diffuse = mix(baseColor.rgb * (1 - dielectric.r), black, metallic);
  vec3 f_0 = mix(dielectric, baseColor.rgb, metallic);
  float alpha = roughness * roughness;

  float VdotH = clamp(dot(V, H), 0., 1.);
  float baseShlickFactor = 1 - VdotH;
  float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
  shlickFactor *= shlickFactor; // power 4
  shlickFactor *= baseShlickFactor; // power 5

  vec3 F = f_0 + (vec3(1.) - f_0) * shlickFactor;


  float NdotL = clamp(dot(N, L), 0., 1.);
  float NdotV = clamp(dot(N, V), 0., 1.);
  float sqrAlpha = alpha * alpha;

  float visibilityDenom = NdotV * sqrt(sqrAlpha + ( 1. - sqrAlpha) * NdotL * NdotL) +
                          NdotL * sqrt(sqrAlpha + ( 1. - sqrAlpha) * NdotV * NdotV);

  float Vis = visibilityDenom > 0. ? 0.5 / visibilityDenom : 0.0;

  float NdotH = clamp(dot(N, H), 0., 1.);

  float distributionDenom = (NdotH * NdotH * (sqrAlpha - 1.) + 1.);
  float D = M_1_PI * sqrAlpha / (distributionDenom * distributionDenom);

  vec3 f_specular = F * Vis * D;

  vec3 diffuse = c_diffuse * M_1_PI;

  vec3 f_diffuse = (1. - F) * diffuse;

  vec3 color = (f_diffuse + f_specular) * uLightIntensity * NdotL;

  vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;

  color += emissive;

  if(uEnableOcclusion == 1){
    float occlusion = texture2D(uOcclusionTexture, vTexCoords).r;
    color = mix(color, color * occlusion, uOcclusionStrength);
  }

  fColor = LINEARtoSRGB(color);
}