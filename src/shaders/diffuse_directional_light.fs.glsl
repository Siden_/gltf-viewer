#version 330
#define PI 3.1415926538

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

out vec3 fColor;

void main()
{
   // Need another normalization because interpolation of vertex attributes does not maintain unit length
   vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
   vec3 BRDF = vec3(1./PI);
   fColor = BRDF * uLightIntensity * dot(viewSpaceNormal, uLightDirection);
}